<?php
/**
 * WordPress için taban ayar dosyası.
 *
 * Bu dosya şu ayarları içerir: MySQL ayarları, tablo öneki,
 * gizli anahtaralr ve ABSPATH. Daha fazla bilgi için
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php düzenleme}
 * yardım sayfasına göz atabilirsiniz. MySQL ayarlarınızı servis sağlayıcınızdan edinebilirsiniz.
 *
 * Bu dosya kurulum sırasında wp-config.php dosyasının oluşturulabilmesi için
 * kullanılır. İsterseniz bu dosyayı kopyalayıp, ismini "wp-config.php" olarak değiştirip,
 * değerleri girerek de kullanabilirsiniz.
 *
 * @package WordPress
 */

// ** MySQL ayarları - Bu bilgileri sunucunuzdan alabilirsiniz ** //
/** WordPress için kullanılacak veritabanının adı */
define( 'DB_NAME', 'akbulbul' );

/** MySQL veritabanı kullanıcısı */
define( 'DB_USER', 'root' );

/** MySQL veritabanı parolası */
define( 'DB_PASSWORD', 'root' );

/** MySQL sunucusu */
define( 'DB_HOST', 'localhost' );

/** Yaratılacak tablolar için veritabanı karakter seti. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Veritabanı karşılaştırma tipi. Herhangi bir şüpheniz varsa bu değeri değiştirmeyin. */
define('DB_COLLATE', '');

/**#@+
 * Eşsiz doğrulama anahtarları.
 *
 * Her anahtar farklı bir karakter kümesi olmalı!
 * {@link http://api.wordpress.org/secret-key/1.1/salt WordPress.org secret-key service} servisini kullanarak yaratabilirsiniz.
 * Çerezleri geçersiz kılmak için istediğiniz zaman bu değerleri değiştirebilirsiniz. Bu tüm kullanıcıların tekrar giriş yapmasını gerektirecektir.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lEu>uLA<+3!cr]~r>sc}OO$;-UU~*iM&pC~t4hLKZ GUIz7-A)g?>}:fU:%*#l</' );
define( 'SECURE_AUTH_KEY',  'L`1-%77u/L3~|K+i[_gmBn_ CTH)23E5+JqY]3F-QD.R#sWA%c,03iP]5sH4H&#3' );
define( 'LOGGED_IN_KEY',    '& 3$r]R~E,c5c&(1_$:<xiXNj~QB>ZvZLL {g%:e]#ty7`3pKY)h`g/d_T=r?>#V' );
define( 'NONCE_KEY',        'AC/^S`r_5Z uY}~aUApA3|zC3A+Bb;_-pwC>jI-QcG*G2Z8/^wSV!.*f|`{FWE%b' );
define( 'AUTH_SALT',        '2[Ico^~$^uO)DTJy}Fy`wZXwSGJN<Hm$?`q(Y^rb,j@g:Y:jZVPQzacoh-!!8jOC' );
define( 'SECURE_AUTH_SALT', '*}&.i-hE+U6|I//JUCF6w76wLkI7>kc*dDHor;$P#ytjtf/+Rlr1V!BX!z*gN,0)' );
define( 'LOGGED_IN_SALT',   'tDw^<TX*{<REk8q+TJPLDQZ)eI<-o_XzG0Sawj1R}6Z alug^4D4JcoY-CnSnx1A' );
define( 'NONCE_SALT',       'FR|BkC}:%?3M6G%?5(5|ZTV(K[c+U_fW|P]kL#swsF2&o 7E.cxbUlZxDg[l|u#2' );
/**#@-*/

/**
 * WordPress veritabanı tablo ön eki.
 *
 * Tüm kurulumlara ayrı bir önek vererek bir veritabanına birden fazla kurulum yapabilirsiniz.
 * Sadece rakamlar, harfler ve alt çizgi lütfen.
 */
$table_prefix = 'wp_';

/**
 * Geliştiriciler için: WordPress hata ayıklama modu.
 *
 * Bu değeri "true" yaparak geliştirme sırasında hataların ekrana basılmasını sağlayabilirsiniz.
 * Tema ve eklenti geliştiricilerinin geliştirme aşamasında WP_DEBUG
 * kullanmalarını önemle tavsiye ederiz.
 */
define('WP_DEBUG', false);

/* Hepsi bu kadar. Mutlu bloglamalar! */

/** WordPress dizini için mutlak yol. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** WordPress değişkenlerini ve yollarını kurar. */
require_once(ABSPATH . 'wp-settings.php');
